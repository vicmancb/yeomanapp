(function(angular, undefined) {
'use strict';

angular.module('udemAppApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);