'use strict';
(function(){

class DetailComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('udemAppApp')
  .component('detail', {
    templateUrl: 'app/detail/detail.html',
    controller: DetailComponent
  });

})();
