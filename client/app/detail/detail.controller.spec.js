'use strict';

describe('Component: DetailComponent', function () {

  // load the controller's module
  beforeEach(module('udemAppApp'));

  var DetailComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    DetailComponent = $componentController('DetailComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
