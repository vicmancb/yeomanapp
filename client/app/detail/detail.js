'use strict';

angular.module('udemAppApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('detail', {
        url: '/detail',
        template: '<detail></detail>'
      });
  });
