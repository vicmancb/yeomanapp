'use strict';
(function(){

class PokemonComponent {
  constructor(pokemonService) {
    this.pokemonService = pokemonService;
    this.message = 'Hello';
  }

  $onInit() {
    this.pokemonService.getPokemones().then(response => {

      console.log("desde Ctrl", response);
    });
  }
}

angular.module('udemAppApp')
  .component('pokemon', {
    templateUrl: 'app/pokemon/pokemon.html',
    controller: PokemonComponent
  });

})();
