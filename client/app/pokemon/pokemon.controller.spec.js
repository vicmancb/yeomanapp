'use strict';

describe('Component: PokemonComponent', function () {

  // load the controller's module
  beforeEach(module('udemAppApp'));

  var PokemonComponent, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, $rootScope) {
    scope = $rootScope.$new();
    PokemonComponent = $componentController('PokemonComponent', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
