'use strict';

angular.module('udemAppApp')
  .service('pokemonService', function ($http) {
    var self = this;

    self.getPokemones = function(){
      var request = $http({
        method: 'GET',
        url: 'http://pokeapi.co/api/v2/pokemon/'
      });

      return request;
    };
  });
