'use strict';

angular.module('udemAppApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('pokemon', {
        url: '/pokemon',
        template: '<pokemon></pokemon>'
      });
  });
