'use strict';

describe('Directive: vicPokemon', function () {

  // load the directive's module and view
  beforeEach(module('udemAppApp'));
  beforeEach(module('app/directives/vicPokemon/vicPokemon.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<vic-pokemon></vic-pokemon>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the vicPokemon directive');
  }));
});
