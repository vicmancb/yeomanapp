'use strict';

angular.module('udemAppApp')
  .directive('vicPokemon', function () {
    return {
      templateUrl: 'app/directives/vicPokemon/vicPokemon.html',
      restrict: 'EA',
      scope : {
        name : '='
      },
      link: function (scope, element, attrs) {
        console.log('scope', scope);
        console.log('element', element);
        console.log('attrs', attrs);
        scope.name += "HOLA HOLA HOLA";
        scope.id = 10;


      }
    };
  });
