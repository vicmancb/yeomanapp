'use strict';

angular.module('udemAppApp')
  .service('mainService', function ($http) {

    var self = this;

    self.getMessages = function(){
      var request = $http({
        method: 'GET',
        url: 'http://udem.herokuapp.com/parcial'
      });

      return request;
    };

  });
