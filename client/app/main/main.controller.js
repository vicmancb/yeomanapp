'use strict';

(function() {

class MainController {

  constructor(mainService) {
    this.mainService = mainService;
    this.messageList = [];
  }

  $onInit() {
    this.mainService.getMessages().then(response => {
      this.messageList = response.data.messages;
      console.log("desde Ctrl", this.messages);
    });
  }
}

angular.module('udemAppApp')
  .component('main', {
    templateUrl: 'app/main/main.html',
    controller: MainController
  });

})();
