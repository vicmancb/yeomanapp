'use strict';

describe('Filter: vicFilter', function () {

  // load the filter's module
  beforeEach(module('udemAppApp'));

  // initialize a new instance of the filter before each test
  var vicFilter;
  beforeEach(inject(function ($filter) {
    vicFilter = $filter('vicFilter');
  }));

  it('should return the input prefixed with "vicFilter filter:"', function () {
    var text = 'angularjs';
    expect(vicFilter(text)).toBe('vicFilter filter: ' + text);
  });

});
