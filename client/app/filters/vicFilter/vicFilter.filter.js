'use strict';

angular.module('udemAppApp')
  .filter('vicFilter', function () {
    return function (input) {
      var concat = "";
      if (input === 1) {
        concat = "st";
      }
      if (input === 2) {
        concat = "nd";
      }
      if (input === 3) {
        concat = "rd";
      }
      return input + concat;
    };
  });
